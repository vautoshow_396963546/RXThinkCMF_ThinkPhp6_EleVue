<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\facade\Db;

/**
 * 行为日志-模型
 * @author 牧羊人
 * @since 2020/11/14
 * Class ActionLog
 * @package app\admin\model
 */
class ActionLog extends BaseModel
{
    // 设置数据表
    protected $table = null;
    // 自定义日志标题
    protected static $title = '';
    // 自定义日志内容
    protected static $content = '';

    /**
     * 构造函数
     * @param array $data
     * @since 2020/11/14
     * ActionLog constructor.
     * @author 牧羊人
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        // 设置表名
        $this->table = DB_PREFIX . 'action_log_' . date('Y') . '_' . date('m');
        // 初始化行为日志表
        $this->initTable();
    }

    /**
     * 初始化日志表
     * @return string|null
     * @throws \think\db\exception\BindParamException
     * @author 牧羊人
     * @since 2020/11/14
     */
    private function initTable()
    {
        $tbl = $this->table;
        if (!$this->tableExists($tbl)) {
            $sql = "CREATE TABLE `{$tbl}` (
                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一性标识',
                  `username` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '操作人用户名',
                  `method` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '请求类型',
                  `module` varchar(30) DEFAULT NULL COMMENT '模型',
                  `url` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '操作页面',
                  `param` text CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '请求参数(JSON格式)',
                  `title` varchar(100) DEFAULT NULL COMMENT '日志标题',
                  `type` tinyint(1) unsigned DEFAULT 0 COMMENT '操作类型：1登录系统 2注销系统 3操作日志',
                  `content` varchar(1000) DEFAULT '' COMMENT '内容',
                  `ip` varchar(18) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'IP地址',
                  `ip_city` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'IP所属城市',
                  `os` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '操作系统',
                  `browser` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '浏览器',
                  `user_agent` varchar(360) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'User-Agent',
                  `create_user` int(11) unsigned DEFAULT 0 COMMENT '添加人',
                  `create_time` int(11) unsigned DEFAULT 0 COMMENT '添加时间',
                  `mark` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '有效标识：1正常 0删除',
                  PRIMARY KEY (`id`) USING BTREE
                ) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT='系统行为日志表' ROW_FORMAT = COMPACT;";
            Db::query($sql);

        }
        return $tbl;
    }

    /**
     * 设置标题
     * @param $title 标题
     * @since 2020/11/14
     * @author 牧羊人
     */
    public static function setTitle($title)
    {
        self::$title = $title;
    }

    /**
     * 设置内容
     * @param $content 内容
     * @since 2020/11/14
     * @author 牧羊人
     */
    public static function setContent($content)
    {
        self::$content = $content;
    }

    /**
     * 创建日志
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 牧羊人
     * @since 2020/11/20
     */
    public static function record()
    {
        if (!self::$title) {
            // 操作控制器名
            $menuModel = new Menu();
            $info = $menuModel->getOne([
                ['path', '=', request()->url()],
            ]);
            if ($info) {
                if ($info['type'] == 1) {
                    $menuInfo = $menuModel->getInfo($info['pid']);
                    self::$title = $menuInfo['title'];
                } else {
                    self::$title = $info['title'];
                }
            }
        }

        // 用户ID
        $userId = 0;
        // 获取Token
        $token = request()->header("Authorization");
        if ($token && strpos($token, 'Bearer ') !== false) {
            $token = str_replace("Bearer ", null, $token);
            // JWT解密token
            $jwt = new \Jwt();
            $userId = $jwt->verifyToken($token);
        }

        // 日志数据
        $data = [
            'username' => '管理员',
            'module' => app('http')->getName(),
            'method' => request()->method(),
            'url' => request()->url(true), // 获取完成URL
            'param' => request()->param() ? json_encode(request()->param()) : '',
            'title' => self::$title ? self::$title : '操作日志',
            'type' => self::$title == '登录系统' ? 1 : (self::$title == '注销系统' ? 2 : 3),
            'content' => self::$content,
            'ip' => request()->ip(),
//            'ip_city' => get_ip_city(request()->ip()),
            'os' => get_os(),
            'browser' => get_browse(),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
            'create_user' => empty($userId) ? 0 : $userId,
            'create_time' => time(),
        ];
        // 日志入库
        self::insert($data);
    }

}