<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\controller;


class Menu2
{
    public function index()
    {
        // 管理员(拥有全部权限)
        $menuModel = new \app\admin\model\Menu();
        $menuList = $menuModel->getChilds(0);
        if ($menuList) {
            foreach ($menuList as $val) {
                $data = [
                    'title' => trim($val['title']),
                    'icon' => trim($val['icon']),
                    'path' => trim($val['path']),
                    'component' => trim($val['component']),
                    'target' => trim($val['target']),
                    'pid' => 0,
                    'type' => intval($val['type']),
                    'permission' => trim($val['permission']),
                    'status' => 1,
                    'sort' => intval($val['sort']),
                    'create_user' => 1,
                    'create_time' => time(),
                    'update_user' => 1,
                    'update_time' => time(),
                ];
                $menuModel = new \app\admin\model\Menu2();
                $result = $menuModel->edit($data);
                if (!empty($val['children'])) {
                    foreach ($val['children'] as $vo) {
                        $data2 = [
                            'title' => trim($vo['title']),
                            'icon' => trim($vo['icon']),
                            'path' => trim($vo['path']),
                            'component' => trim($vo['component']),
                            'target' => trim($vo['target']),
                            'pid' => $result,
                            'type' => intval($vo['type']),
                            'permission' => trim($vo['permission']),
                            'status' => 1,
                            'sort' => intval($vo['sort']),
                            'create_user' => 1,
                            'create_time' => time(),
                            'update_user' => 1,
                            'update_time' => time(),
                        ];
                        $menuModel = new \app\admin\model\Menu2();
                        $result2 = $menuModel->edit($data2);
                        if (!empty($vo['children'])) {
                            foreach ($vo['children'] as $v) {
                                $data3 = [
                                    'title' => trim($v['title']),
                                    'icon' => trim($v['icon']),
                                    'path' => trim($v['path']),
                                    'component' => trim($v['component']),
                                    'target' => trim($v['target']),
                                    'pid' => $result2,
                                    'type' => intval($v['type']),
                                    'permission' => trim($v['permission']),
                                    'status' => 1,
                                    'sort' => intval($v['sort']),
                                    'create_user' => 1,
                                    'create_time' => time(),
                                    'update_user' => 1,
                                    'update_time' => time(),
                                ];
                                $menuModel = new \app\admin\model\Menu2();
                                $menuModel->edit($data3);
                            }
                        }
                    }
                }

            }
        }
        print_r($menuList);
        exit;
    }
}